require_relative 'query_param_builders/base_param_builder'
require_relative 'query_param_builders/date_param_builder'
require_relative 'query_param_builders/labels_param_builder'

module Gitlab
  module Triage
    module GraphqlQueries
      class QueryBuilder
        def initialize(source_type, resource_type, conditions, graphql_only: false)
          @source_type = source_type.to_s.singularize
          @resource_type = resource_type
          @conditions = conditions
          @graphql_only = graphql_only
        end

        def resource_path
          [source_type, resource_type]
        end

        def query
          return if resource_fields.empty?

          format(
            BASE_QUERY,
            source_type: source_type,
            resource_type: resource_type.to_s.camelize(:lower),
            resource_fields: resource_fields.join(' '),
            resource_query: resource_query,
            iids_declaration: graphql_only ? nil : ', $iids: [String!]',
            iids_query: graphql_only ? nil : ', iids: $iids'
          )
        end

        delegate :any?, to: :resource_fields

        private

        attr_reader :source_type, :resource_type, :conditions, :graphql_only

        BASE_QUERY = <<~GRAPHQL.freeze
          query($source: ID!, $after: String%{iids_declaration}) {
            %{source_type}(fullPath: $source) {
              id
              %{resource_type}(after: $after%{iids_query}%{resource_query}) {
                pageInfo {
                  hasNextPage
                  endCursor
                }
                nodes {
                  id iid title updatedAt createdAt webUrl projectId %{resource_fields}
                }
              }
            }
          }
        GRAPHQL

        def resource_fields
          fields = []

          fields << 'userNotesCount' if conditions.dig(:discussions, :attribute).to_s == 'notes'
          fields << 'userDiscussionsCount' if conditions.dig(:discussions, :attribute).to_s == 'threads'

          if graphql_only
            fields << 'labels { nodes { title } }'
            fields << 'author { id name username }'
            fields << 'assignees { nodes { id name username } }' if conditions.key?(:assignee_member)
            fields << 'upvotes' if conditions.dig(:upvotes, :attribute).to_s == 'upvotes'
            fields << 'downvotes' if conditions.dig(:upvotes, :attribute).to_s == 'downvotes'
            fields.push('draft', 'mergedAt') if resource_type == 'merge_requests'
          end

          fields
        end

        def resource_query
          condition_queries = []

          condition_queries << QueryParamBuilders::BaseParamBuilder.new('includeSubgroups', true, with_quotes: false) if source_type == 'group'

          conditions.each do |condition, condition_params|
            condition_queries << QueryParamBuilders::DateParamBuilder.new(condition_params) if condition.to_s == 'date'
            condition_queries << QueryParamBuilders::BaseParamBuilder.new('milestoneTitle', condition_params) if condition.to_s == 'milestone'
            condition_queries << QueryParamBuilders::BaseParamBuilder.new('state', condition_params, with_quotes: false) if condition.to_s == 'state'

            case resource_type
            when 'issues'
              condition_queries << issues_label_query(condition, condition_params)
            when 'merge_requests'
              condition_queries << merge_requests_label_query(condition, condition_params)
              condition_queries << merge_requests_resource_query(condition, condition_params)
            end
          end

          condition_queries
            .compact
            .map(&:build_param)
            .join
        end

        def issues_label_query(condition, condition_params)
          args =
            case condition.to_s
            when 'forbidden_labels'
              ['labelName', condition_params, { negated: true }]
            when 'labels'
              ['labelName', condition_params]
            else
              return nil
            end

          QueryParamBuilders::LabelsParamBuilder.new(*args)
        end

        def merge_requests_resource_query(condition, condition_params)
          args =
            case condition.to_s
            when 'source_branch'
              ['sourceBranches', condition_params]
            when 'target_branch'
              ['targetBranches', condition_params]
            when 'draft'
              ['draft', condition_params, { with_quotes: false }]
            else
              return nil
            end

          QueryParamBuilders::BaseParamBuilder.new(*args)
        end

        def merge_requests_label_query(condition, condition_params)
          args =
            case condition.to_s
            when 'forbidden_labels'
              ['labels', condition_params, { negated: true }]
            when 'labels'
              ['labels', condition_params]
            else
              return nil
            end

          QueryParamBuilders::LabelsParamBuilder.new(*args)
        end
      end
    end
  end
end
